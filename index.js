const express = require('express')

// default port is 8000 if env var PORT is not defined
const port = process.env.PORT || 8000;

// init express
const app = express();

// endpoints/route handlers
app.get('/', (req, res) => {
  res.send('Hello World!')
});

// listen on a port
app.listen(port, () => {
  console.log(`Server is listening on port ${port}!`)
});

